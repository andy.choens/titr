# esquisse.R
# This script installs the esquisse package (if necessary) and then runs it 
## against the passengers data set we have worked with during this workshop.
## To run the script hit either the "Run" or "Source" buttons in the top-right
## corner of this script window.

## SETUP =======================================================================
if (!require(esquisse)) install.packages("esquisse")
library(esquisse)
library(tidyverse)



## DATA ========================================================================
passengers <- read_csv("www/passengers.csv")



## ANALYSIS ====================================================================
esquisser(passengers)