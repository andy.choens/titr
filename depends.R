packages_to_install <- 
  c(
    "knitr",
    "learnr",
    "shiny",
    "tidymodels",
    "tidyverse")
install.packages(packages_to_install)