# Titanic Introduction To R (TITR)

<img src="./images/data-science-life-cycle.png" style="float:right;width:50%">

Welcome to an all new TITR, a nautical journey through the cold depths of the R programming language. This course takes you from zero to <strike>hero</strike> cold in just a few hours. Each session comes with a set of interactive tutorials which you can complete on your own OR during our workshop. The accompanying exercises provide a bit more learning and a chance to challenge yourself further.

Our journey is based on Hadley Wickham's famous data science life cycle. Each lesson will cover one or more topics in the cycle. Some parts of the cycle have more than one lesson.

## Before we begin

1. Complete the [setup](setup.Rmd) tutorial.
2. Dependencies: If you still need to install R-packages needed for this course, open [depends.R](./depends.R) in RStudio and run this script via the "Run" or "Source" buttons or copy/paste from the browser into the R console/REPL. To open depends.R, use the "Files" portion of the RStudio IDE interface:

<img src="./images/rstudio-files.png" style="display:block;margin-left:auto;margin-right:auto;float:center;width:33%;">

3. At the end of today's workshop, please complete my [satisfaction survey](https://docs.google.com/forms/d/e/1FAIpQLSdOIYxJ0VhtyAarQ9KVB9kiL2cNZX0TcXCu_B5fTRbucJGsVg/viewform).
4. If you experienced any bugs with the workshop content or have concrete suggestions, please submit a [bug/feature request here](https://gitlab.com/andy.choens/titr/-/issues).

<br />
<span style="color:red;font-size:20px;">If you do not have R and RStudio installed on your computer, please let us know now!</span>
<br /><br />

## Icebergs Ahead! An Introduction to R

Tutorial and exercise notebooks in this section start with the word `icebergs`.

Files which end in `.Rmd` are R Markdown files, and include a minimalist markup syntax and R code chunks. Files which end in `.R` are R script files which contain only R code and comments (lines starting with `#`). You can open files using the files part of the RStudio interface (see picture above).

### Workshop Materials

1. [Intro](icebergs-intro.Rmd)
2. [Import, Tidy, Transform](icebergs-import-tidy-transform.Rmd)
    - [Filter](icebergs-filter.Rmd)
    - [Mutate, Summary](icebergs-mutate-summary.Rmd)
3. [Communicate, Visualize](icebergs-communicate.Rmd)


## Women & Children First! First Regression in R

Tutorial and exercise notebooks in this section start with the word `women`.

- COMING SOON

## Maiden Voyage: First R Project

Tutorial and exercise notebooks in this section start with the word `voyage`.

- COMING SOON